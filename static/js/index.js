/**
 * Created by nishtha on 7/3/15.
 */

var input = $('#input-a');
input.clockpicker({
    autoclose: true,
    placement: 'right'
});

var input = $('#input-b');
input.clockpicker({
    autoclose: true,
    placement: 'right'
});

function verify() {
    var time1 = document.getElementById('input-a').value;
    var time2 = document.getElementById('input-b').value;
    var source = document.getElementById('input-0').value;
    var dest = document.getElementById('input-1').value;
    var time;
    if (source == "Source location") {
        window.alert("Enter source");
        return false;
    } else if (dest == "Destination location") {
        window.alert("Select destination");
        return false;
    } else if (time1 == "Leave at") {
        window.alert("Select depart time");
        return false;
    } else if (time2 == "Reach before") {
        window.alert("Select arrival time");
        return false;
    } else {
        var h1, h2, m1, m2;
        h1 = parseInt(time1.substring(0, 2));
        m1 = parseInt(time1.substring(3, 5));
        h2 = parseInt(time2.substring(0, 2));
        m2 = parseInt(time2.substring(3, 5));
        if (h1 > h2) {
            window.alert('Depart time not less than Arrival time');
            return false;
        } else if (h1 == h2 && m1 > m2) {
            window.alert('Depart time not less than Arrival time');
            return false;
        } else {
            time = (60 - m1) + m2 + ((h2 - h1 - 1) * 60);
            time = time * 60;
        }
    }
    return true;
};