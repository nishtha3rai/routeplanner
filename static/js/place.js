/**
 * Created by nishtha on 7/3/15.
 */

var types;
var markers = [];
var places = {};
var _directionsRenderer;
var directionsService;
var org;
var des;
var reverse_places = {};

$('.searchable').multiSelect({
    selectableHeader: "<input type='text' style='width:90%; background: orange; margin-bottom:0px; color: white;' autocomplete='off' placeholder='Type category'>",
    selectionHeader: "<input type='text' style='width:90%; background: orange; margin-bottom:0px; color: white;' autocomplete='off' placeholder='Selected'>",
    afterInit: function(ms){
        var that = this,
            $selectableSearch = that.$selectableUl.prev(),
            $selectionSearch = that.$selectionUl.prev(),
            selectableSearchString = '#'+that.$container.attr('id')+' .ms-elem-selectable:not(.ms-selected)',
            selectionSearchString = '#'+that.$container.attr('id')+' .ms-elem-selection.ms-selected';
        that.qs1 = $selectableSearch.quicksearch(selectableSearchString).on('keydown', function(e){
            if (e.which === 40){
                that.$selectableUl.focus();
                return false;
            }
        });

        that.qs2 = $selectionSearch.quicksearch(selectionSearchString).on('keydown', function(e){
            if (e.which == 40){
                that.$selectionUl.focus();
                return false;
            }
        });
    },
    afterSelect: function(){
        this.qs1.cache();
        this.qs2.cache();
    },
    afterDeselect: function(){
        this.qs1.cache();
        this.qs2.cache();
    }
});
$('#custom-headers').multiSelect();

function getTypes() {
    var cat = document.getElementById('custom-headers').options;
    var cat_arr = [];

    for (var j = 0; j < markers.length; j++) {
        markers[j].setMap(null);
    }
    markers = [];

    for (var j = 0; j < cat.length; j++) {
        if (cat[j].selected) {
            cat_arr.push(cat[j].value);
        }
    }
    types = cat_arr;
}

function addMarker(lat, lng, name) {
    var latlng = new google.maps.LatLng(lat, lng);

    var marker = new google.maps.Marker({
        position: latlng,
        map: map,
        title: name
    });
    google.maps.event.addListener(marker, 'click', function() {
        if (marker.title in places) {
            window.alert("Place already added");
        } else {
            var minutes = window.prompt("Please enter time to spend in minutes:", "0");
            if (minutes == null) {
                window.alert("Place not added, reselect place!");
                return;
            }
            places[marker.title] = [marker.position.lat(), marker.position.lng(), parseInt(minutes)];
            var str = marker.position.lat() + ',' + marker.position.lng();
            reverse_places[str] = marker.title;
            var div = document.getElementById('place_list');
            var button = document.createElement('DIV');
            var close = document.createElement('BUTTON');
            close.onclick = function(event) {
                var str = event.currentTarget.parentNode.innerHTML;
                var ltln = places[str.substring(0, str.indexOf(','))];
                delete places[str.substring(0, str.indexOf(','))];
                delete reverse_places[ltln];
                event.currentTarget.parentNode.parentNode.removeChild(event.currentTarget.parentNode);
            };
            close.className = 'close_button';
            button.className = 'place_button';
            button.innerHTML = marker.title + ', ' + minutes;
            button.appendChild(close);
            div.appendChild(button);
        }
    });

    markers.push(marker);
}

function initialize() {
    org = new google.maps.LatLng(lat1, lng1);
    des = new google.maps.LatLng(lat2, lng2);
    directionsService = new google.maps.DirectionsService();
    _directionsRenderer = new google.maps.DirectionsRenderer();
    var LegPoints = new Array();

    var myOptions = {
        zoom: 7,
        center: org,
        mapTypeId: google.maps.MapTypeId.ROADMAP
    };
    map = new google.maps.Map(document.getElementById("map_canvas"), myOptions);

    _directionsRenderer.setMap(map);
    _directionsRenderer.setOptions({
        draggable: true
    });
    var request = {
        origin:org,
        destination:des,
        travelMode: google.maps.DirectionsTravelMode.DRIVING
    };
    directionsService.route(request, function(result, status) {
        if (status == google.maps.DirectionsStatus.OK){
            _directionsRenderer.setDirections(result);
        }
    });
}

function displayCategories() {
    var service = new google.maps.places.PlacesService(map);
    var request_place = {
        location: new google.maps.LatLng(lat1, lng1),
        radius: total_dist,
        types: types
    };
    service.nearbySearch(request_place, callback);
    for (var j = 0.1; j < 0.6; j += 0.1) {
        var request_place = {
            location: new google.maps.LatLng(lat1 + j, lng1 + j),
            radius: total_dist,
            types: types
        };
        service.nearbySearch(request_place, callback);
        var request_place = {
            location: new google.maps.LatLng(lat1 - j, lng1 - j),
            radius: total_dist,
            types: types
        };
        service.nearbySearch(request_place, callback);
        var request_place = {
            location: new google.maps.LatLng(lat1 + j, lng1 - j),
            radius: total_dist,
            types: types
        };
        service.nearbySearch(request_place, callback);
        var request_place = {
            location: new google.maps.LatLng(lat1 - j, lng1 + j),
            radius: total_dist,
            types: types
        };
        service.nearbySearch(request_place, callback);
    }
    var request_place = {
        location: new google.maps.LatLng(lat2, lng2),
        radius: total_dist,
        types: types
    };
    service.nearbySearch(request_place, callback);
    for (var j = 0.1; j < 0.6; j += 0.1) {
        var request_place = {
            location: new google.maps.LatLng(lat2 + j, lng2 + j),
            radius: total_dist,
            types: types
        };
        service.nearbySearch(request_place, callback);
        var request_place = {
            location: new google.maps.LatLng(lat2 - j, lng2 - j),
            radius: total_dist,
            types: types
        };
        service.nearbySearch(request_place, callback);
        var request_place = {
            location: new google.maps.LatLng(lat2 + j, lng2 - j),
            radius: total_dist,
            types: types
        };
        service.nearbySearch(request_place, callback);
        var request_place = {
            location: new google.maps.LatLng(lat2 - j, lng2 + j),
            radius: total_dist,
            types: types
        };
        service.nearbySearch(request_place, callback);
    }
}

function callback(results, status) {
    if (status == google.maps.places.PlacesServiceStatus.OK) {
        for (var i = 0; i < results.length; i++) {
            addMarker(results[i]['geometry']['location']['k'], results[i]['geometry']['location']['D'], results[i]['name']);
        }
    }
}

function getRoute() {
    var waypts = [];
    var keys = Object.keys(places);
    for (var j = 0; j < keys.length; j++) {
        var obj = {};
        obj['location'] = new google.maps.LatLng(places[keys[j]][0], places[keys[j]][1]);
        obj['stopover'] = true;
        waypts.push(obj);
    }
    var request = {
        origin:org,
        destination:des,
        waypoints: waypts,
        optimizeWaypoints: true,
        travelMode: google.maps.DirectionsTravelMode.DRIVING
    };
    directionsService.route(request, function(result, status) {
        if (status == google.maps.DirectionsStatus.OK){
            var route = result.routes[0];
            var order = route.waypoint_order;
            var total_dur = 0;

            for (var j = 0; j < route.legs.length - 1; j++) {
                var str = waypts[j]['location']['k'] + ',' + waypts[j]['location']['D'];
                total_dur += route.legs[j]['duration']['value'];
                total_dur += places[reverse_places[str]][2] * 60;
            }
            total_dur += route.legs[route.legs.length - 1]['duration']['value'];
            if (sec < total_dur) {
                window.alert("Time for travelling more than user-specified range. Please change place or time for place");
            } else {
                _directionsRenderer.setDirections(result);
            }
        }
    });
}