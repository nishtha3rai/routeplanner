from flask import Flask, render_template, request, url_for, flash
import requests

app = Flask(__name__)
app.secret_key = "sodium_ff"

@app.route('/', methods=['GET', 'POST'])
def index():
    if request.method == 'GET':
        return render_template('index.html')
    else:
        source = request.form['source']
        time1 = request.form['time1']
        dest = request.form['dest']
        time2 = request.form['time2']
        json_data = requests.get('https://maps.googleapis.com/maps/api/directions/json?origin=' + source + '&destination=' + dest).json()
        print json_data
        sec = json_data['routes'][0]['legs'][0]['duration']['value']
        lat1 = json_data['routes'][0]['legs'][0]['start_location']['lat']
        lng1 = json_data['routes'][0]['legs'][0]['start_location']['lng']
        lat2 = json_data['routes'][0]['legs'][0]['end_location']['lat']
        lng2 = json_data['routes'][0]['legs'][0]['end_location']['lng']
        user_sec = (60 - int(time1[3:])) + int(time2[3:]) + ((int(time2[:2]) - int(time1[:2]) - 1) * 60)
        user_sec *= 60
        print user_sec, sec
        if sec > user_sec + 200:
            flash('Duration not feasible for source and destination')
            return render_template('index.html')
        else:
            return render_template('place.html', sec=user_sec, source=source, desination=dest, depart=time1, arrive=time2, lat1=lat1, lng1=lng1, lat2=lat2, lng2=lng2)

if __name__ == '__main__':
    app.run(threaded=True)